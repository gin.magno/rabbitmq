# Rabbitmq

## Setup

1. Create folders for config, data and logs

```
mkdir appl
mkdir appl/hipms
mkdir -p appl/hipms/rabbitmq/config appl/hipms/rabbitmq/data appl/hipms/rabbitmq/logs
```

2. Create rabbitmq.conf

```
cd appl/hipms/rabbitmq/config
```

```
vi rabbitmq.conf
```

3. Add the following to rabbitmq.conf

```
loopback_users = none
loopback_users.guest = false
listeners.tcp.default = 5672
default_user = hipmsadm
default_pass = welcome1
hipe_compile = false
vm_memory_high_watermark.relative = 0.8
log.dir = /var/log/rabbitmq
log.file = rabbit.log
log.console = false
log.file.level = info
log.connection.level = info
cluster_formation.peer_discovery_backend = rabbit_peer_discovery_classic_config
cluster_formation.classic_config.nodes.1 = rabbit@vmhipmsapd26
cluster_formation.classic_config.nodes.2 = rabbit@vmhipmsapd27
cluster_formation.classic_config.nodes.3 = rabbit@vmhipmsapd28
cluster_formation.classic_config.nodes.4 = rabbit@vmhipmsapd29
cluster_formation.classic_config.nodes.5 = rabbit@vmhipmsapd30
cluster_partition_handling.pause_if_all_down.recover = autoheal
cluster_partition_handling = autoheal
cluster_formation.node_type = disc
cluster_partition_handling.pause_if_all_down.nodes.1 = rabbit@vmhipmsapd26
cluster_partition_handling.pause_if_all_down.nodes.2 = rabbit@vmhipmsapd27
cluster_partition_handling.pause_if_all_down.nodes.3 = rabbit@vmhipmsapd28
cluster_partition_handling.pause_if_all_down.nodes.4 = rabbit@vmhipmsapd29
cluster_partition_handling.pause_if_all_down.nodes.5 = rabbit@vmhipmsapd30
mnesia_table_loading_retry_limit = 10
mnesia_table_loading_retry_timeout = 30000
```

4. Create plugins

```
vim enabled_plugins
```

5. Add the following to enabled_plugins

```
[rabbitmq_federation,rabbitmq_management].
```

6. Deploy with docker compose

**Note**: Make sure you are on the root folder

```
docker compose up -d
```

---

## Access RabbitMQ Console

1. To access the RabbitMQ console, go to <http://localhost:15672>

user: guest \
password: guest

---

## Troubleshooting

If for some reason the other nodes aren't shown in the management console, simply delete the ./appl/hipms/rabbitmq/data/ and ./appl/hipms/rabbitmq/logs/ folder

---

## Related Docs

<https://docs.google.com/document/d/1SyPKwfsEtq00vSuwha8oB2FdjadyQCjK/edit>
